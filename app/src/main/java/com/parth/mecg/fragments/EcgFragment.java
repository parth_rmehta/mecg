package com.parth.mecg.fragments;


import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.parth.mecg.R;
import com.parth.mecg.utils.Constants;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import butterknife.InjectView;

/**
 * A fragment with a Google +1 button.
 * Use the {@link EcgFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EcgFragment extends Fragment {

    public static String TAG = "EcgFragment";
    public static long max_noise_threshold = 4000;
    public static long min_noise_threshold = 0;
    private int HISTORY_SIZE = 300;
    private long prv, curr, nxt;
    private static LinkedList<Long> ecgRaLlHistory = new LinkedList<Long>();

    private Button btnPlotECG;
    @InjectView(R.id.dynamicXYPlot)
    XYPlot ecgPlot;
    private SimpleXYSeries aLvlSeries;


    public static EcgFragment newInstance(String param1, String param2) {
        EcgFragment fragment = new EcgFragment();

        return fragment;
    }

    public EcgFragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ecg, container, false);
        ecgPlot = (XYPlot) view.findViewById(R.id.dynamicXYPlot);

        btnPlotECG = (Button) view.findViewById(R.id.btnPlotECG);

        btnPlotECG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PlotECG().execute();
            }
        });

        initializeGraph();

        return view;
    }


    public void initializeGraph() {

        ecgPlot.setDomainStepValue(5);
        ecgPlot.setTicksPerRangeLabel(3);
        ecgPlot.setDomainLabel("Time");
        ecgPlot.getDomainLabelWidget().pack();
        ecgPlot.setRangeLabel("Level");
        ecgPlot.getRangeLabelWidget().pack();
        //ecgPlot.setDomainBoundaries(0,HISTORY_SIZE , BoundaryMode.FIXED);
        ecgPlot.setRangeBoundaries(1200, 3200, BoundaryMode.GROW);
        ecgPlot.setGridPadding(5, 0, 5, 0);
        //ecgPlot.setRangeValueFormat(new Long());
        //ecgPlot.getGraphWidget().setDomainValueFormat(new DecimalFormat());

        ecgPlot.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        ecgPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);

        ecgPlot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
        ecgPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

        ecgPlot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        ecgPlot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

        ecgPlot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        ecgPlot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        ecgPlot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);
        ecgPlot.getLegendWidget().getTextPaint().setColor(Color.BLACK);

        aLvlSeries = new SimpleXYSeries("ECG");

        LineAndPointFormatter formatter =
                new LineAndPointFormatter(Color.rgb(200, 0, 0), null, null, null);
        formatter.getLinePaint().setStrokeWidth(7);
        formatter.getLinePaint().setStrokeJoin(Paint.Join.ROUND);

        ecgPlot.addSeries(aLvlSeries, formatter);

    }

    public class PlotECG extends AsyncTask<Void, Long, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getActivity(), "Plotting ECG", Toast.LENGTH_SHORT).show();


        }

        @Override
        protected Void doInBackground(Void... voids) {

            AssetManager assetManager = getResources().getAssets();
            InputStream inputStream = null;

            try {
                inputStream = assetManager.open(Constants.FILE_NAME);
                if (inputStream != null) {
                    Log.d(TAG, "File resource found");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line = reader.readLine();
                    long val = 0;
                    while (line != null && (Double.valueOf(line.trim()).longValue() > 0)) {

                        val = Double.valueOf(line.trim()).longValue();
                        onProgressUpdate(val);
                        line = reader.readLine();

                    }

                }
            } catch (Exception e) {

                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);

            if (ecgRaLlHistory.size() > HISTORY_SIZE) {

                ecgRaLlHistory.removeFirst();

                ecgRaLlHistory.addLast(noiseReduction(values[0]));
                aLvlSeries.setModel(ecgRaLlHistory,
                        SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
                ecgPlot.redraw();
            } else {
                ecgRaLlHistory.addLast(noiseReduction(values[0]));

                aLvlSeries.setModel(ecgRaLlHistory,
                        SimpleXYSeries.ArrayFormat.Y_VALS_ONLY);
            }

        }

        protected long noiseReduction(long nxt) {

            if ((min_noise_threshold < nxt) && (nxt < max_noise_threshold)) {

                if (ecgRaLlHistory.size() > 3) {

                    long mean = (nxt + ecgRaLlHistory.get(ecgRaLlHistory.size() - 1) + ecgRaLlHistory.get(ecgRaLlHistory.size() - 2)) / 3; //(nxt +   ecgRaLlHistory.get(ecgRaLlHistory.size() - 2)  +   ecgRaLlHistory.get(ecgRaLlHistory.size() - 3)  +  ecgRaLlHistory.get(ecgRaLlHistory.size() - 4)  + ecgRaLlHistory.get(ecgRaLlHistory.size() - 5)) / 5;
                    Log.d(TAG, nxt + " -- " + mean);
                    return Math.abs(mean - nxt) < 150 ? mean : nxt;
                } else
                    return nxt;
            } else
                return 0;

        }


    }


}
