package com.parth.mecg.fragments;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parth.mecg.R;
import com.parth.mecg.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment
 *
 */
public class SplashScreen extends Fragment {

    public static SplashScreen newInstance(String param1, String param2) {
        SplashScreen fragment = new SplashScreen();

        return fragment;
    }
    public SplashScreen() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */
            @Override
            public void run() {
                Constants.changeScreen(getActivity(), Constants.SCREEN_ECG, false, null);
            }
        }, Constants.SPLASH_TIME_OUT);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash_screen, container, false);
    }



}
