package com.parth.mecg;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parth.mecg.fragments.EcgFragment;
import com.parth.mecg.fragments.SplashScreen;
import com.parth.mecg.utils.Constants;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private int currentScreenId;
    public BroadcastReceiver screenChangeListner;
    ContentResolver mResolver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().hide();
        createBroadcastReceivers();
        registerBroadcastReceivers();


        if (savedInstanceState == null) {

            Constants.changeScreen(this, Constants.SCREEN_SPLASH,
                    false, null);

        }

        Constants.mAccount = Constants.CreateSyncAccount(MainActivity.this);

        if (Constants.mAccount != null) {

            mResolver = getContentResolver();
            Log.i(TAG, "setting reSolver");
            mResolver.setSyncAutomatically(Constants.mAccount, Constants.AUTHORITY, true);

        }
    }


    public void registerBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                screenChangeListner,
                new IntentFilter(Constants.CHANGE_SCREEN_BROADCAST));
    }

    public void createBroadcastReceivers() {
        screenChangeListner = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int screenId = intent.getIntExtra(Constants.KEY_SCREEN_NO, -1);
                Bundle bundle = intent.getExtras();
                boolean addToBackstack = intent.getBooleanExtra(
                        Constants.KEY_ADD_TO_BACKSTACK, false);
                currentScreenId = screenId;
                Fragment fragment = null;


                switch (screenId) {
                    case Constants.SCREEN_SPLASH:
                        fragment = new SplashScreen();
                        changeScreenFragment(fragment, addToBackstack, bundle);
                        break;
                    case Constants.SCREEN_ECG:
                        fragment = new EcgFragment();
                        Log.e(TAG, "ECG Fragment");
                        changeScreenFragment(fragment, addToBackstack, bundle);
                        break;
                    default:
                        break;
                }

            }
        };
    }

    public void changeScreenFragment(Fragment frag, boolean addToBackstack,
                                     Bundle b) {
        if (b != null) {
            frag.setArguments(b);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);
        ft.replace(R.id.container, frag);

        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);

        ft.commitAllowingStateLoss();

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }


}
