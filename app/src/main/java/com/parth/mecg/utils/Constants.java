package com.parth.mecg.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Parth on 24-09-2014.
 */
public class Constants {

    public static final String TAG = "Constants";
    public static final String KEY_SCREEN_NO = "screen_no";
    public static final String KEY_ADD_TO_BACKSTACK = "add_to_backstack";


    public static final int SCREEN_SPLASH = 1;
    public static final int SCREEN_ECG = 2;

    public static final String CHANGE_SCREEN_BROADCAST = "com.parth.mecg.CHANGE_SCREEN_BROADCAST";


    public static final int SPLASH_TIME_OUT = 2000;

    public static final String FILE_NAME = "Sample_ECG.dat";


    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.parth.mecg.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "mecg.com";
    // The account name
    public static final String ACCOUNT = "TestAccount";
    private static final long SYNC_FREQUENCY = 60 * 60;  // 1 hour (in seconds)
    // Instance fields
   public static Account mAccount;


    public static String PREF_SETUP_COMPLETE = "key_account_setup";


    public static void changeScreen(Context context, int screenNo,
                                    boolean addtoBackstack, Bundle bundle) {

        Intent intent = new Intent(CHANGE_SCREEN_BROADCAST);
        if (bundle != null)
            intent.putExtras(bundle);
        intent.putExtra(KEY_SCREEN_NO, screenNo);
        intent.putExtra(KEY_ADD_TO_BACKSTACK, addtoBackstack);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }


    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {


        boolean newAccount = false;
        boolean setupComplete = PreferenceManager
                .getDefaultSharedPreferences(context).getBoolean(PREF_SETUP_COMPLETE, false);

        Account mAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        try {
            // Create the account type and default account

            // Get an instance of the Android account manager
            AccountManager accountManager =
                    (AccountManager) context.getSystemService(
                            context.ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (accountManager.addAccountExplicitly(mAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */

                // Inform the system that this account supports sync
                ContentResolver.setIsSyncable(mAccount, AUTHORITY, 1);
                // Inform the system that this account is eligible for auto sync when the network is up
                ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
                // Recommend a schedule for automatic synchronization. The system may modify this based
                // on other scheduled syncs and network utilization.
                ContentResolver.addPeriodicSync(
                        mAccount, AUTHORITY, new Bundle(), SYNC_FREQUENCY);
                newAccount = true;


            } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */

            }
            return mAccount;
        } catch (Exception exp) {
            exp.printStackTrace();
            return mAccount;
        }


    }


}
