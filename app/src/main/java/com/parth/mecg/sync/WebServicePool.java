package com.parth.mecg.sync;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Parth on 22-10-2014.
 */
public class WebServicePool {

    public static String URL = "http://127.0.0.1:007/";
    public static String URL_sendECGData = "send-ecg";

    public static void sendECGData(ArrayList<String> data, Context context) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        Gson gson = new GsonBuilder().create();

        JSONArray a = new JSONArray(data);
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("ecg", a);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(URL + URL_sendECGData, jsonData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // mPostCommentResponse.requestCompleted();
                Log.i("sendECGData", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mPostCommentResponse.requestEndedWithError(error);
                Log.i("sendECGData", error.toString());
            }
        });

        // Add the request to the RequestQueue.
        queue.add(jsonObjRequest);
    }

}
